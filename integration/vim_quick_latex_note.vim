if exists("g:loaded_vim_quick_latex_note")
	finish
endif
let g:loaded_vim_quick_latex_note = 1

if !has('python3')
	echo "[Vim Quick LaTeX Note] Error: Required vim compiled with +python3"
	finish
endif

if executable("quick_note")
	let s:binary = "quick_note"
elseif executable("./quick_note")
	let s:binary = "./quick_note"
else
	echo "[Vim Quick LaTeX Note] Error: quick_note not found"
	finish
endif

let s:path = expand('%:p:h')

function! s:InsertQuickNote()
" Python code starts
python3 << EOF
import vim
import os

# TODO: replace 'note' it with function argument?
filename = ["note", 0, ".png"]
wd = vim.eval("s:path") + "/"

while os.path.exists(wd + "".join(str(_) for _ in filename)):
	filename[1] += 1

fname = "".join(str(_) for _ in filename)

vim.command("silent !clear")
vim.command("silent !" + vim.eval("s:binary") + " -f " + wd + fname)
vim.command("redraw!")

if os.path.exists(wd + fname):
	current_line, current_col = vim.current.window.cursor
	vim.current.window.cursor = current_line, 0

	vim.current.buffer.append("\\begin{figure}[h]", current_line)
	current_line += 1

	vim.current.buffer.append("\\centering", current_line)
	current_line += 1

	vim.current.buffer.append("\\includegraphics[width=0.7\\textwidth]{" + fname + "}", current_line)
	current_line += 1

	vim.current.buffer.append("\\caption{Note " + str(filename[1]) + "}", current_line)
	current_line += 1

	vim.current.buffer.append("\\end{figure}", current_line)
	current_line += 1

EOF
" Python code ends

endfunction

command! VQLNInsertNote call s:InsertQuickNote()
