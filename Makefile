CXX=clang++
CXXFLAGS= -std=c++14 -Wall
CXXFLAGS+=${shell pkg-config gtkmm-3.0 --cflags}
LIBS=${shell pkg-config gtkmm-3.0 --libs}

OBJDIR = objects
SRCDIR = source

SOURCES=$(wildcard $(SRCDIR)/*.cpp)
OBJECTS=$(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

.PHONY: clean install

TARGET=quick_note

all: $(TARGET)

debug: CXXFLAGS += -g -DQN_DEBUG
debug: $(TARGET)

quick_note: $(OBJECTS)
	@$(CXX) $(CXXFLAGS) -o $@ $(OBJECTS) $(LIBS)

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@$(CXX) -c $(CXXFLAGS) $< -o $@

clean:
	@- rm $(OBJDIR)/*.o
	@- rm quick_note

install:
ifeq ("$(wildcard $(TARGET))","") 
	@echo "Nothing to install, build application first with make"
else
	@install -m 755 $(TARGET) /usr/bin/
endif
