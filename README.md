# Quick Note
### application for quick hand drawn sketches and notes with command-line arguments support

Quick Note uses gtkmm3, C++ bindings for GTK3, which is licensed under GNU LGPL.

**this app is still in development, although it is pretty much usable by now.**

## Motivation
This app was created because I needed quick and fast way to insert hand-drawn image in my LaTeX lecture notes.
Since no graphic editor supported command-line arguments for filename, I created this app.

In integration directory you will find example of vim plugin to quickly find last untaken filename a-la
'noteN.png', run Quick Note with this filename, and insert LaTeX code to use this image.

## Screenshot

![cool screnshot](screenshot.png?raw=true)
