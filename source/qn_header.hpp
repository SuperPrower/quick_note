#pragma once

#include <gtkmm/headerbar.h>
#include <gtkmm/button.h>

class QuickNoteHeaderBar : public Gtk::HeaderBar {
public:
	QuickNoteHeaderBar();
	~QuickNoteHeaderBar() = default;

private:
	friend class QuickNoteWindow;

	Gtk::Button new_button;
	Gtk::Button open_button;
	Gtk::Button save_button;
};
