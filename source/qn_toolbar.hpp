#pragma once

#include <gtkmm/box.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/colorbutton.h>

enum class Tools {
	PEN,
	LINE,
	ERASER
};

class QuickNoteToolbar : public Gtk::Box {
public:
	QuickNoteToolbar();
	~QuickNoteToolbar() = default;

protected:
	void qn_on_pen_selected();
	void qn_on_line_selected();
	void qn_on_eraser_selected();

private:
	friend class QuickNoteDrawingArea;

	Gtk::ToggleButton 	pen, line, eraser;
	Tools			selected_tool;

	Gtk::Button		clear_button;
	Gtk::ColorButton 	color_button;

	Gtk::Box 		empty_space;
};
