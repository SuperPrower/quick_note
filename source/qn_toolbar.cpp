#include "qn_toolbar.hpp"

QuickNoteToolbar::QuickNoteToolbar() {

	this->set_size_request(-1, 48);

	/* Add Icons and Signal Handlers to Tool Buttons */
	// TODO: Set proper icons
	pen.set_image_from_icon_name("format-text-direction-rtl");
	pen.set_tooltip_text("Pen");
	pen.set_size_request(48, 48);
	pen.signal_toggled().connect(
			sigc::mem_fun(*this, &QuickNoteToolbar::qn_on_pen_selected)
	);

	line.set_image_from_icon_name("object-flip-horizontal");
	line.set_tooltip_text("Line: Hold to draw straight line from touch point to release point");
	line.set_size_request(48, 48);
	line.signal_toggled().connect(
			sigc::mem_fun(*this, &QuickNoteToolbar::qn_on_line_selected)
	);

	eraser.set_image_from_icon_name("insert-object");
	eraser.set_tooltip_text("Eraser");
	eraser.set_size_request(48, 48);
	eraser.signal_toggled().connect(
			sigc::mem_fun(*this, &QuickNoteToolbar::qn_on_eraser_selected)
	);

	clear_button.set_image_from_icon_name("edit-clear-all");
	clear_button.set_tooltip_text("Clear. Press to clear canvas");
	clear_button.set_size_request(48, 48);
	// Handler for Clear Button will be assigned in Drawing Area

	color_button.set_size_request(48, 48);
	color_button.set_color(color_button.get_color());

	this->pack_start(pen, false, false);
	this->pack_start(line, false, false);
	this->pack_start(eraser, false, false);

	this->pack_start(color_button, false, false);

	// Set Pen selected by default
	pen.set_active(true);

	// Add some empty space?
	empty_space.set_vexpand(true);
	this->pack_start(empty_space, true, true);

	this->pack_end(clear_button, false, false);
}

void QuickNoteToolbar::qn_on_pen_selected() {

	/*
	 * Those functions work like following:
	 * 1. If this button was activated, disable other two and set active tool
	 * 2. Else, check if other two buttons are disabled
	 * 3. If so, that means we pressed on already selected button - re-enable it again
	 * 4. Else ignore, because that means we pressed on another button
	 */

	if (this->pen.get_active()) {
		this->line.set_active(false);
		this->eraser.set_active(false);

		this->selected_tool = Tools::PEN;

	} else if (!this->line.get_active() && !this->eraser.get_active()) {
		this->pen.set_active(true);
	}
}

void QuickNoteToolbar::qn_on_line_selected() {
	if (this->line.get_active()) {
		this->pen.set_active(false);
		this->eraser.set_active(false);

		this->selected_tool = Tools::LINE;

	} else if (!this->pen.get_active() && !this->eraser.get_active()) {
		this->line.set_active(true);
	}
}

void QuickNoteToolbar::qn_on_eraser_selected() {
	if (this->eraser.get_active()) {
		this->pen.set_active(false);
		this->line.set_active(false);

		this->selected_tool = Tools::ERASER;

	} else if (not this->pen.get_active() && not this->line.get_active()) {
		this->eraser.set_active(true);
	}
}
