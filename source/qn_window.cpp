#include "qn_window.hpp"

#include <gtkmm/filechooserdialog.h>

#include <iostream>

QuickNoteWindow::QuickNoteWindow(std::string filename)
	: drawing_area(toolbar)
	, filename(filename)
{
	/* setup window */
	this->set_title("Quick Note");
	this->set_default_size(600, 600);
	this->set_resizable(false);

	/* add header bar */
	this->set_titlebar(header_bar);
	this->set_title("Quick Note");
	this->header_bar.set_title("Quick Note");
	this->qn_update_subtitle();

	/* Bind callbacks to Header Bar buttons */
	header_bar.new_button.signal_clicked().connect(
		sigc::mem_fun(*this, &QuickNoteWindow::qn_on_new)
	);

	header_bar.open_button.signal_clicked().connect(
		sigc::mem_fun(*this, &QuickNoteWindow::qn_on_load)
	);

	header_bar.save_button.signal_clicked().connect(
		sigc::mem_fun(*this, &QuickNoteWindow::qn_on_save)
	);

	/* limit toolbar and separator expansion */
	separator.set_vexpand(false);
	separator.set_hexpand(true);
	separator.set_halign(Gtk::Align::ALIGN_FILL);
	separator.set_valign(Gtk::Align::ALIGN_START);

	toolbar.set_vexpand(false);
	toolbar.set_hexpand(true);
	toolbar.set_halign(Gtk::Align::ALIGN_FILL);
	toolbar.set_valign(Gtk::Align::ALIGN_START);

	/* add everything to container */
	box.pack_start(toolbar);
	box.pack_start(separator);
	box.pack_start(drawing_area);

	this->add(box);

	this->show_all();

	/* If filename was provided as argument, try to open it */
	if (not filename.empty())
		drawing_area.qn_load_surface(filename);
}

QuickNoteWindow::~QuickNoteWindow() {}


void QuickNoteWindow::qn_on_new() {
#ifdef QN_DEBUG
	std::cout << "Creating new file" << std::endl;
#endif

	filename = std::string();
	this->qn_update_subtitle();
	drawing_area.qn_on_clear_button_clicked();
}

void QuickNoteWindow::qn_on_load() {
	/* Create FileChooser Dialog to select file */
	Gtk::FileChooserDialog dialog(
			*this, "Load Image",
			Gtk::FileChooserAction::FILE_CHOOSER_ACTION_OPEN
	);

	dialog.add_button("Cancel", Gtk::RESPONSE_CANCEL);
	dialog.add_button("Open", Gtk::RESPONSE_ACCEPT);

	// Set PNG image filter
	Glib::RefPtr<Gtk::FileFilter> image_filter = Gtk::FileFilter::create();
	image_filter->set_name("PNG files");
	image_filter->add_mime_type("image/png");

	dialog.add_filter(image_filter);

	if (dialog.run() == Gtk::RESPONSE_ACCEPT) {
		filename = dialog.get_filename();
	} else {
		return;
	}

#ifdef QN_DEBUG
	std::cout << "Trying to open file: " << filename << std::endl;
#endif
	this->qn_update_subtitle();

	// Proceed to actually open file
	drawing_area.qn_load_surface(filename);
}

void QuickNoteWindow::qn_on_save() {
	if (filename.empty()) {
		/* Create FileChooser Dialog to select file */
		Gtk::FileChooserDialog dialog(
				*this, "Save Image",
				Gtk::FileChooserAction::FILE_CHOOSER_ACTION_SAVE
		);

		dialog.add_button("Cancel", Gtk::RESPONSE_CANCEL);
		dialog.add_button("Save", Gtk::RESPONSE_ACCEPT);

		// Set PNG image filter
		Glib::RefPtr<Gtk::FileFilter> image_filter = Gtk::FileFilter::create();
		image_filter->set_name("PNG files");
		image_filter->add_mime_type("image/png");

		dialog.add_filter(image_filter);

		if (dialog.run() == Gtk::RESPONSE_ACCEPT) {
			filename = dialog.get_filename();
		} else {
			return;
		}

	}
#ifdef QN_DEBUG
	std::cout << "Saving file: " << filename << std::endl;
#endif
	this->qn_update_subtitle();

	// Proceed to actually save file
	drawing_area.qn_save_surface(filename);
}

void QuickNoteWindow::qn_update_subtitle() {
	std::string sub_title = filename.empty() ? "untitled" : filename;
	this->header_bar.set_subtitle(sub_title);
}
