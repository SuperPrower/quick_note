#pragma once

#include "qn_header.hpp"
#include "qn_toolbar.hpp"
#include "qn_drawing_area.hpp"

#include <gtkmm/window.h>
#include <gtkmm/separator.h>

#include <string>

class QuickNoteWindow : public Gtk::Window {
public:
	QuickNoteWindow(std::string file_name);
	virtual ~QuickNoteWindow();

protected:
	void qn_on_new();
	void qn_on_load();
	void qn_on_save();

	void qn_update_subtitle();

private:
	QuickNoteHeaderBar 	header_bar;
	QuickNoteToolbar	toolbar;
	QuickNoteDrawingArea	drawing_area;

	Gtk::VBox		box;
	Gtk::VSeparator		separator;

	// File, to which results should be saved
	std::string 		filename;

};
