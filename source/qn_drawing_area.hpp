#pragma once

#include "qn_toolbar.hpp"

#include <gtkmm/drawingarea.h>
#include <gdkmm/event.h>

#include <vector>
#include <utility>

struct DrawnPath {
	DrawnPath(Gdk::RGBA color, Tools tool);
	DrawnPath();

	void clear();

	float 		colors[4];
	Tools		tool;
	std::vector<std::pair<float, float>>	path;
	bool		finished;
	bool		valid;
};

class QuickNoteDrawingArea : public Gtk::DrawingArea {
public:
	QuickNoteDrawingArea(QuickNoteToolbar & toolbar);
	virtual ~QuickNoteDrawingArea();

protected:
	bool on_draw(const ::Cairo::RefPtr<::Cairo::Context> & cr) override;
	bool on_button_press_event(GdkEventButton *) override;
	bool on_motion_notify_event (GdkEventMotion *) override;
	bool on_button_release_event (GdkEventButton *) override;

	void qn_on_clear_button_clicked();

	friend class QuickNoteWindow;

	void qn_load_surface(std::string filename);
	void qn_save_surface(std::string filename);

private:
	/* Line or path that is currently being draw
	 * Being saved on mouse release
	 * to avoid frequent surfaces copies
	 */
	DrawnPath drawing;

	// Surface to which drawings are being saved
	Cairo::RefPtr<Cairo::ImageSurface> surface;

	// Reference to toolbar is used to get current color and selected tool
	QuickNoteToolbar& toolbar;
};
