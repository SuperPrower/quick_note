#include "qn_header.hpp"

QuickNoteHeaderBar::QuickNoteHeaderBar() {
	// Show window controls
	set_show_close_button(true);

	/* Create Buttons */
	new_button.set_image_from_icon_name("document-new");
	open_button.set_image_from_icon_name("document-open");
	save_button.set_image_from_icon_name("document-save");

	pack_start(new_button);
	pack_start(open_button);
	pack_start(save_button);

}
