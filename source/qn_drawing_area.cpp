#include "qn_drawing_area.hpp"

#include <iostream>

#define PRINT_POS 0 // enable this to print mouse events

DrawnPath::DrawnPath(Gdk::RGBA color, Tools tool) : tool(tool), finished(false), valid(true) {
	colors[0] = color.get_red();
	colors[1] = color.get_green();
	colors[2] = color.get_blue();
	colors[3] = color.get_alpha();
}

DrawnPath::DrawnPath() : valid(false) {}

void DrawnPath::clear() {
	path.clear();
	valid = false;
}

QuickNoteDrawingArea::QuickNoteDrawingArea(QuickNoteToolbar & toolbar)
	: drawing()
	, toolbar(toolbar)
{

	this->add_events(Gdk::BUTTON_PRESS_MASK);
	this->add_events(Gdk::BUTTON1_MOTION_MASK);
	this->add_events(Gdk::BUTTON_RELEASE_MASK);

	this->set_size_request(600, 600);
	this->set_halign(Gtk::Align::ALIGN_CENTER);
	this->set_valign(Gtk::Align::ALIGN_CENTER);

	// Assign handler to clear button
	toolbar.clear_button.signal_clicked().connect(
		sigc::mem_fun(*this, &QuickNoteDrawingArea::qn_on_clear_button_clicked)
	);

	surface.clear();

}

bool QuickNoteDrawingArea::on_draw(const Cairo::RefPtr<Cairo::Context> & cr) {
	/* fill background with white */

	auto allocation = this->get_allocation();

	if (!surface) {
#ifdef QN_DEBUG
		std::cout << "Surface doesn't exist, creating it" << std::endl;
#endif

		/*
		surface = this->get_window()->create_similar_image_surface(
			Cairo::Format::FORMAT_ARGB32,
			allocation.get_width(),
			allocation.get_height(),
			this->get_window()->get_scale_factor()
		);
		*
		* doesn't work because method returns regular surface,
		* https://bugzilla.gnome.org/show_bug.cgi?id=793572
		*/

		surface = Cairo::ImageSurface::create(
			Cairo::Format::FORMAT_ARGB32,
			allocation.get_width(),
			allocation.get_height()
		);

		/* fill it with white */
		Cairo::RefPtr<Cairo::Context> t_context = Cairo::Context::create(surface);
		t_context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
		t_context->paint();

	}

	cr->save();
	cr->set_source(surface, 0.0, 0.0);
	cr->paint();
	cr->restore();

	/* draw lines (eraser is just thick white lines) */
	if (drawing.valid) {
		cr->save();
		switch (drawing.tool) {
			case Tools::PEN:
			case Tools::LINE:
				cr->set_source_rgba(
					drawing.colors[0],
					drawing.colors[1],
					drawing.colors[2],
					drawing.colors[3]
				);
				cr->set_line_width(3.0);
				break;

			case Tools::ERASER:
				cr->set_source_rgba(1.0, 1.0, 1.0, 1.0);
				cr->set_line_width(15.0);
		}

		cr->begin_new_path();
		for (auto & dot : drawing.path) {
			cr->line_to(dot.first, dot.second);
		}
		cr->stroke();
		cr->restore();

		// On mouse button release, save surface
		if (drawing.finished) {
			// Create new Cairo Context with saved texture,
			// and draw onto it to save updated texture
			Cairo::RefPtr<Cairo::Context> t_context = Cairo::Context::create(surface);
			/* XXX: I don't know why you have to consider allocation coordinates,
			 * but it just like that. Without them, picture is going to slide
			 * every time surface is saved-restored.
			 */
			t_context->set_source(cr->get_target(), -allocation.get_x(), -allocation.get_y());
			t_context->paint();

			drawing.clear();
		}
	}


	return false;
}

bool QuickNoteDrawingArea::on_button_press_event(GdkEventButton * button_event) {
#ifdef QN_DEBUG
#if PRINT_POS
	std::cout
		<< "Button Press at: "
		<< "\t"
		<< button_event->x
		<< "\t"
		<< button_event->y
		<< std::endl;

#endif // PRINT_POS
#endif // QN_DEBUG

	if (button_event->button == 1) {
		drawing = DrawnPath(
			toolbar.color_button.get_rgba(),
			toolbar.selected_tool
		);
	}

	return false;
}

bool QuickNoteDrawingArea::on_motion_notify_event (GdkEventMotion* motion_event) {
#ifdef QN_DEBUG
#if PRINT_POS
	std::cout
		<< "Motion at: "
		<< "\t"
		<< motion_event->x
		<< "\t"
		<< motion_event->y
		<< std::endl;
#endif // PRINT_POS
#endif // QN_DEBUG

	if (drawing.valid) {
		switch (drawing.tool) {
			case Tools::LINE:
				// TODO: Add shift support
				if (!drawing.path.empty())
					drawing.path.pop_back();
				else
					drawing.path.emplace_back(
							motion_event->x,
							motion_event->y
						);

			case Tools::PEN:
			case Tools::ERASER:
				drawing.path.emplace_back(
						motion_event->x,
						motion_event->y
					);
		}

		this->queue_draw();

	}

	return false;
}

bool QuickNoteDrawingArea::on_button_release_event (GdkEventButton * button_event)  {
#ifdef QN_DEBUG
#if PRINT_POS
	std::cout
		<< "Button Release at: "
		<< "\t"
		<< button_event->x
		<< "\t"
		<< button_event->y
		<< std::endl;

#endif // PRINT_POS
#endif // QN_DEBUG
	if (drawing.valid && button_event->button == 1) {
		switch (drawing.tool) {
			case Tools::LINE:
				if (!drawing.path.empty())
					drawing.path.pop_back();
				else
					drawing.path.emplace_back(
						button_event->x,
						button_event->y
					);

			case Tools::PEN:
			case Tools::ERASER:
				drawing.path.emplace_back(
					button_event->x,
					button_event->y
				);
		}

		drawing.finished = true;

		this->queue_draw();
	}

	return false;
}

void QuickNoteDrawingArea::qn_load_surface(std::string filename) {
	if (surface) {
		surface.clear();
	}

	try {
		surface = Cairo::ImageSurface::create_from_png(filename);
	} catch (const Cairo::logic_error & e) {
#ifdef QN_DEBUG
		// XXX: I forgot what exactly happens here. Is this a problem? Add more explanations.
		std::cout << "Unable to open file, ignoring" << std::endl;
		std::cout << e.what() << std::endl;
#endif
	}
}

void QuickNoteDrawingArea::qn_save_surface(std::string filename) {
	if (!surface) {
		std::cout << "Surface doesn't exist. Try to draw something before saving" << std::endl;
	} else {
		surface->write_to_png(filename);
	}
}

void QuickNoteDrawingArea::qn_on_clear_button_clicked() {
	surface.clear();
	this->queue_draw();
}

QuickNoteDrawingArea::~QuickNoteDrawingArea() {}
