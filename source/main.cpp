#include "qn_window.hpp"

#include <gtkmm.h>

#include <unistd.h>
#include <getopt.h>

#include <iostream>

const std::string version = "Quick Note 0.1 © 2018 Sergey Koziakov";

const std::string help =
"Usage: quick_note [-f filename] [-v] [-h]\n"
"\t-v, --version\t\tdisplay version and exit\n"
"\t-h, --help\t\tdisplay this message and exit\n"
"\t-f, --filename <file>\t\topen or create <file>";


int main(int argc, char **argv) {
	/* parse command-line arguments */

	int opt, longoptind = 0;
	std::string filename;

	struct option longopts[] = {
		{"verion", no_argument, NULL, 'v'},
		{"help", no_argument, NULL, 'h'},
		{"filename", required_argument, NULL, 'f'}
	};

	while ((opt = getopt_long(argc, argv, "vhf:", longopts, &longoptind)) != -1) {
		switch(opt) {
		case 'v':
			std::cout << version << std::endl;
			return 0;
		case 'h':
			std::cout << version << std::endl;
			std::cout << help << std::endl;
			return 0;
		case 'f':
			filename = optarg;
			break;
		}
	}

	auto app = Gtk::Application::create(
			// argc, argv,
			"superprower.quick_note"
	);

	QuickNoteWindow window(filename);

	return app->run(window);
}
